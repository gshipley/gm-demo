package org.openshift.mlbparks.rest;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.openshift.mlbparks.domain.GMDealership;
import org.openshift.mlbparks.mongo.DBConnection;

import com.mongodb.BasicDBObject;

@RequestScoped
@Path("/parks")
public class MLBParkResource {

	@Inject
	private DBConnection dbConnection;

	private MongoCollection getMLBParksCollection() {
		MongoDatabase db = dbConnection.getDB();
		MongoCollection<Document> parkListCollection = db.getCollection("teams");

		return parkListCollection;
	}

	private GMDealership populateParkInformation(Document dataValue) {
		GMDealership theDealership = new GMDealership();
		theDealership.setName(dataValue.get("Store Name"));
		theDealership.setPosition(dataValue.get("pos"));
		theDealership.setId(dataValue.get("_id").toString());
		theDealership.setLatitude(dataValue.get("Latitude"));
		theDealership.setLongitude(dataValue.get("Longitude"));

		return theDealership;
	}

	// get all the mlb parks
	@GET()
	@Produces("application/json")
	public List<GMDealership> getAllParks() {
		ArrayList<GMDealership> allParksList = new ArrayList<GMDealership>();
		System.out.println("Hi..");
		MongoCollection mlbParks = this.getMLBParksCollection();
		MongoCursor<Document> cursor = mlbParks.find().iterator();
		try {
			while (cursor.hasNext()) {
				allParksList.add(this.populateParkInformation(cursor.next()));
			}
		} finally {
			cursor.close();
		}

		return allParksList;
	}

	@GET
	@Produces("application/json")
	@Path("within")
	public List<GMDealership> findParksWithin(@QueryParam("lat1") float lat1,
											  @QueryParam("lon1") float lon1, @QueryParam("lat2") float lat2,
											  @QueryParam("lon2") float lon2) {

	    // The first thing we want to do is make sure the DB contains data
        dbConnection.checkDatabase();
		ArrayList<GMDealership> allParksList = new ArrayList<GMDealership>();
		MongoCollection mlbParks = this.getMLBParksCollection();

		// make the query object
		BasicDBObject spatialQuery = new BasicDBObject();

		ArrayList<double[]> boxList = new ArrayList<double[]>();
		boxList.add(new double[] { new Float(lon2), new Float(lat2) });
		boxList.add(new double[] { new Float(lon1), new Float(lat1) });

		BasicDBObject boxQuery = new BasicDBObject();
		boxQuery.put("$box", boxList);

		spatialQuery.put("pos", new BasicDBObject("$within", boxQuery));
		System.out.println("Using spatial query: " + spatialQuery.toString());

		MongoCursor<Document> cursor = mlbParks.find(spatialQuery).iterator();
		try {
			while (cursor.hasNext()) {
				allParksList.add(this.populateParkInformation(cursor.next()));
			}
		} finally {
			cursor.close();
		}

		return allParksList;
	}
}
